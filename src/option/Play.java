package option;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;

import Controler.Option_setting;
import Controler.dictionnaireSelecteur;
import Controler.gagnerControl;
import Controler.perdreControl;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import principale.GestionJeu;

public class Play {

	private int choixImage; //on a trois theme ,pour chaque theme ,on definit trois int variables
	private int choixNiveau; //changer le difficulte  (1-beginner, 2-normal, 3-export)
	private int choixFont; //changer le size du mot  entre (15 - 30)
	private Option_setting parametre;
	private Stage primaryStage;
	
	private URL musicLigne;
	private Media music;
	private MediaPlayer backgroundMusic;
	private dictionnaireSelecteur selector;
	private GestionJeu jeu;
	
	@FXML private GridPane _fxPlay;  //la root
	@FXML ImageView _stickman;  // l'image du bonhomme
	@FXML HBox _pane;   // la root des lettres
	private ArrayList<GridPane> listLettre;   // la liste des lettres
	
	@FXML Label _erreur;  // le nombre d'erreur cree
	@FXML Label _text;   // quelque chose a dire...
	
	@FXML private Button _music;  // la musique
	
	@FXML private Label _section; // pour afficher la categorie
	
	// des boutons a,b,c,d...........
	private ArrayList<Button> listButton;
	@FXML private Button _a;
	@FXML private Button _b;
	@FXML private Button _c;
	@FXML private Button _d;
	@FXML private Button _e;
	@FXML private Button _f;
	@FXML private Button _g;
	@FXML private Button _h;
	@FXML private Button _i;
	@FXML private Button _j;
	@FXML private Button _k;
	@FXML private Button _l;
	@FXML private Button _m;
	@FXML private Button _n;
	@FXML private Button _o;
	@FXML private Button _p;
	@FXML private Button _q;
	@FXML private Button _r;
	@FXML private Button _s;
	@FXML private Button _t;
	@FXML private Button _u;
	@FXML private Button _v;
	@FXML private Button _w;
	@FXML private Button _x;
	@FXML private Button _y;
	@FXML private Button _z;
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	//                                              Constructeur
	///////////////////////////////////////////////////////////////////////////////////////////////
	public Play(Option_setting parametre, GestionJeu jeu, dictionnaireSelecteur selecteur, Stage primaryStage) throws IOException {
		this.parametre = parametre;
		this.jeu = jeu;
		jeu.InitialiserPartie();
		
		this.choixImage = parametre.getChoixImage();
		this.choixFont = parametre.getChoixFont();
		this.choixNiveau = parametre.getChoixNiveau();
		
		this.selector = selecteur;
		this.primaryStage = primaryStage;

		switch (choixImage) {
		case 1:
			musicLigne = getClass().getResource("/music1_2.mp3");
			music = new Media(musicLigne.toString());
			break;
		case 2:
			musicLigne = getClass().getResource("/music2_2.mp3");
			music = new Media(musicLigne.toString());
			break;
		case 3:
			musicLigne = getClass().getResource("/music3_2.mp3");
			music = new Media(musicLigne.toString());
			break;
		default:
			break;
		}
		backgroundMusic = new MediaPlayer(music);
		backgroundMusic.play();
		backgroundMusic.setCycleCount(MediaPlayer.INDEFINITE);
		backgroundMusic.setVolume(0.6);
	}

	@FXML
	private void initialize() {
		// on prend une dictionnaire
		_section.setText(selector.getCategorie());
		_section.setFont(new Font("Century Gothic", choixFont));
		
		Image stick;
		
		// on initialise la musique
		if(backgroundMusic.isMute()) {
			_music.setId("music_on");
			backgroundMusic.setMute(false);
		}
		
		_music.setId("music_on");
		// on initialise le background
		switch (choixImage) {
		case 1:
			_fxPlay.setId("pane1");
			stick = new Image(this.getClass().getResource("/stickman1_1.png").toString());
			_stickman.setImage(stick);
			_stickman.setStyle("-fx-effect: dropshadow(three-pass-box, black, 10, 0.8, 0, 0);");
			_stickman.setFitHeight(425);
			_stickman.setFitWidth(230.5);
			break;
		case 2:
			_fxPlay.setId("pane2");
			stick = new Image(this.getClass().getResource("/stickman2_1.png").toString());
			_stickman.setImage(stick);
			_stickman.setStyle("-fx-effect: dropshadow(three-pass-box, white, 30, 0.8, 0, 0);");
			break;
		case 3:
			_fxPlay.setId("pane3");
			stick = new Image(this.getClass().getResource("/stickman3_1.png").toString());
			_stickman.setImage(stick);
			_stickman.setStyle("-fx-effect: dropshadow(three-pass-box, white, 10, 0.8, 0, 0);");
			_stickman.setFitHeight(418);
			_stickman.setFitWidth(145.2);
			break;
		default:
			break;
		}
		
		// on initialise afficher le nombre d'erreurs
		_erreur.setText("Chance : 4/4 left");
		_erreur.setFont(new Font("Century Gothic", choixFont));
		_text.setFont(new Font("Century Gothic", choixFont));
		
		// on  initialise nombre de lettres a deviner
		listLettre = new ArrayList<>();
		for(int i = 0; i < jeu.getMotMystere().length(); i++) {
			GridPane gPane = new GridPane();
			
			Image underline = null;
			if(choixImage == 1) {
				underline = new Image(this.getClass().getResource("/underline1.png").toString());
				gPane.setStyle("-fx-effect: dropshadow(three-pass-box, black, 5, 0.8, 0, 0);");
			}
			if(choixImage == 2) {                                                                            
				underline = new Image(this.getClass().getResource("/underline.png").toString());
				gPane.setStyle("-fx-effect: dropshadow(three-pass-box, #7BEE88, 5, 0.8, 0, 0);");
			}
			if(choixImage == 3) {                                                                            
				underline = new Image(this.getClass().getResource("/underline1.png").toString());
				gPane.setStyle("-fx-effect: dropshadow(three-pass-box, #dc4425, 5, 0.8, 0, 0);");
			}
			ImageView im = new ImageView(underline);
			Label nu = new Label();
			nu.setMinHeight(50);
			gPane.add(nu, 0, 0);
			gPane.add(im, 0, 1);
			
			_pane.getChildren().add(gPane);
			listLettre.add(gPane);
		}
		
		
		
		// on cree une liste des buttons
		listButton = new ArrayList<Button>();
		listButton.add(_a);
		listButton.add(_b);
		listButton.add(_c);
		listButton.add(_d);
		listButton.add(_e);
		listButton.add(_f);
		listButton.add(_g);
		listButton.add(_h);
		listButton.add(_i);
		listButton.add(_g);
		listButton.add(_j);
		listButton.add(_k);
		listButton.add(_l);
		listButton.add(_m);
		listButton.add(_n);
		listButton.add(_o);
		listButton.add(_p);
		listButton.add(_q);
		listButton.add(_r);
		listButton.add(_s);
		listButton.add(_t);
		listButton.add(_u);
		listButton.add(_v);
		listButton.add(_w);
		listButton.add(_x);
		listButton.add(_y);
		listButton.add(_z);
		
		for(Button button: listButton) {
			button.setFont(new Font("Century Gothic", choixFont));
			switch (choixImage) {
			case 1:
				button.setId("button1");
				break;
			case 2:
				button.setId("button2");
				break;
			case 3:
				button.setId("buttonS3");
				break;
			default:
				break;
			}
		}
	}

	

	public MediaPlayer getBackgroundMusic() {
		return backgroundMusic;
	}

	public GridPane get_fxPlay() {
		return _fxPlay;
	}

	public void set_fxPlay(GridPane _fxPlay) {
		this._fxPlay = _fxPlay;
	}

	public int getChoixImage() {
		return choixImage;
	}


	public int getChoixNiveau() {
		return choixNiveau;
	}

	public int getChoixFont() {
		return choixFont;
	}
	

	
	/**
	 * on initialise l'image du bonhomme quand choisir un lettre incorrect
	 * @param nbErreurs
	 * @param background
	 * @return image
	 */
	private Image initialiserImage(int nbErreurs, int background) {
		musicFalse();
		Image image = null;
		switch (nbErreurs) {
		case 1:
			if(choixImage == 1)
				image = new Image(this.getClass().getResource("/stickman1_2.png").toString());
			if(choixImage == 2)
				image = new Image(this.getClass().getResource("/stickman2_2.png").toString());
			if(choixImage == 3)
				image = new Image(this.getClass().getResource("/stickman3_2.png").toString());
			_erreur.setText("Chance : 3/4 left");
			_text.setText("Ooops! ");
			break;
		case 2:
			if(choixImage == 1)
				image = new Image(this.getClass().getResource("/stickman1_3.png").toString());
			if(choixImage == 2)
				image = new Image(this.getClass().getResource("/stickman2_3.png").toString());
			if(choixImage == 3)
				image = new Image(this.getClass().getResource("/stickman3_3.png").toString());
			_erreur.setText("Chance : 2/4 left");
			_text.setText("Ah, again !");
			break;
		case 3:
			if(choixImage == 1)
				image = new Image(this.getClass().getResource("/stickman1_5.png").toString());
			if(choixImage == 2)
				image = new Image(this.getClass().getResource("/stickman2_5.png").toString());
			if(choixImage == 3)
				image = new Image(this.getClass().getResource("/stickman3_5.png").toString());
			_erreur.setText("Chance : 1/4 left");
			_text.setText("Ooops, be careful !");
			break;
		case 4:
			if(choixImage == 1)
				image = new Image(this.getClass().getResource("/stickman1_6.png").toString());
			if(choixImage == 2)
				image = new Image(this.getClass().getResource("/stickman2_6.png").toString());
			if(choixImage == 3)
				image = new Image(this.getClass().getResource("/stickman3_6.png").toString());
			_erreur.setText("Chance : 0/4 left");
			_text.setText("OMG, last chance left !");
			break;
		default:
			break;
		}
		return image;
		
	}
	
	/**
	 *  on affiche les lettres corrects
	 * @param listLettre
	 * @param pos
	 * @param c
	 */
	private void LettreCorrect(ArrayList<GridPane> listLettre, Vector<Integer> pos, char c) {
		musicTrue();
		for(int i:pos) {
			GridPane lettre = listLettre.get(i);
			Label label = new Label(c+"");
			lettre.add(label, 0, 0);
			label.setFont(new Font("Century Gothic", choixFont));
			GridPane.setHalignment(label, HPos.CENTER);
		}
		if(jeu.getNbLettresTrouvees() > jeu.getMotMystere().length() - 2)
			_text.setText("Almost get it !");
		else if(pos.size() == 1)
			_text.setText("HAHA, you're right !");
		else if(pos.size() == 2)
			_text.setText("Well done !");
		else
			_text.setText("Can't believe that ! Amazing !");
	}
	
	/**
	 * on affiche la fenetre de gagner et initialiser la fenetre play
	 */
	private void Gagner() {
		backgroundMusic.pause();
		_erreur.setText("You win !!");
		for(Button b:listButton) {
			b.setDisable(true);
		}
		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controler/Ganger.fxml"));
		gagnerControl gagner = null;
		try {
			gagner = new gagnerControl(parametre, jeu.getMotMystere(), primaryStage);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		loader.setController(gagner);
		GridPane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.show();
		backgroundMusic.stop();
		InitialiserFenetre();
	}
	
	/**
	 *  on affiche la fenetre de perdre et initialiser la fenetre play
	 */
	private void Perdre() {
		backgroundMusic.pause();
		for(Button b:listButton) {
			b.setDisable(true);
		}

		FXMLLoader loader = new FXMLLoader(getClass().getResource("/Controler/Perdre.fxml"));
		perdreControl perdre = null;
		try {
			perdre = new perdreControl(parametre, jeu.getMotMystere(), primaryStage);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		loader.setController(perdre);
		GridPane pane = null;
		try {
			pane = loader.load();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(pane);
		primaryStage.setScene(scene);
		primaryStage.show();
		backgroundMusic.stop();
		InitialiserFenetre();  // initialiser la partie
	}
	
	/**
	 * on initialise la fenetre play
	 */
	private void InitialiserFenetre() {
		for(Button b: listButton) {
			b.setDisable(false);
		}
		_text.setText("");
		listLettre.clear();
		listButton.clear();
		_pane.getChildren().clear();
		
		
		jeu.InitialiserPartie();
		initialize();
		_section.setText(selector.getCategorie());
		_section.setFont(new Font("Century Gothic", choixFont));
	}
	
	// set la musique pour des boutons
	private void musicTrue() {
		MediaPlayer trueSound = new MediaPlayer(new Media(this.getClass().getResource("/true.mp3").toString()));
		trueSound.play();
	}
	private void musicFalse() {
		MediaPlayer falseSound = new MediaPlayer(new Media(this.getClass().getResource("/false.mp3").toString()));
		falseSound.play();
	}
	
	@FXML
	private void ButtonAction(ActionEvent e) {
		Button b = (Button)e.getSource();
		b.setDisable(true);
		Vector<Integer> pos = new Vector<Integer>();
		if(jeu.ChercherLettreDansMot(b.getText().charAt(0), pos) == 0) { //la lettre n'est pas dans le mot, une erreur de plus
			jeu.MAJNbErreurs();
			b.setId("false");
			if(jeu.MaxErreursDepasse()) { //le joueur a depasse le nombre masimum d'erreurs autorise...il perd
				_stickman.setImage(initialiserImage(jeu.getNbErreurs(), choixImage));
				Perdre();
			}
			else {
				_stickman.setImage(initialiserImage(jeu.getNbErreurs(), choixImage));
			}
		}
		else {  //la lettre est dans le mot à toutes les positions indiques dans pos
			b.setId("true");
			if(jeu.ToutTrouve()) { //toutes les lettres ont ete trouves
				LettreCorrect(listLettre, pos, b.getText().charAt(0));
				Gagner();
			}
			else {
				LettreCorrect(listLettre, pos, b.getText().charAt(0));
			}
		}
	}
	


	@FXML
	private void Music() {
		if(backgroundMusic.isMute()) {
			_music.setId("music_on");
			backgroundMusic.setMute(false);
		}
		else {
			_music.setId("music_off");
			backgroundMusic.setMute(true);
		}
	}

}
