package option;


import Controler.Option_setting;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ButtonSetting {

	private Option_setting parametre;
//faire le lien pour FXML
	@FXML ToggleGroup group;
	@FXML RadioButton _image1;
	@FXML RadioButton _image2;
	@FXML RadioButton _image3;
	@FXML ImageView _back1;
	@FXML ImageView _back2;
	@FXML ImageView _back3;
	
	@FXML Slider _font;
	
	@FXML ToggleGroup group1;
	@FXML RadioButton _niveau1;
	@FXML RadioButton _niveau2;
	@FXML RadioButton _niveau3;
	
	@FXML Label _mot1;
	@FXML Label _mot2;
	@FXML Label _mot3;
	@FXML GridPane _backPane;
	
	public ButtonSetting(Option_setting parametre) {
		super();
		this.parametre = parametre;
		this.group = new ToggleGroup();
		this.group1 = new ToggleGroup();
	}
	
	/**
	 * mettre a jour
	 */
	@FXML
	private void initialize() {
		_back1.setImage(new Image(this.getClass().getResource("/home1.png").toString()));
		_back2.setImage(new Image(this.getClass().getResource("/home2.png").toString()));
		_back3.setImage(new Image(this.getClass().getResource("/home3.png").toString()));
		
		_niveau1.setId("buttonSet");
		_niveau2.setId("buttonSet");
		_niveau3.setId("buttonSet");
		// set font size
		_font.setValue(parametre.getChoixFont());
		_mot1.setFont(new Font("Century Gothic", parametre.getChoixFont()));
    	_mot2.setFont(new Font("Century Gothic", parametre.getChoixFont()));
    	_mot3.setFont(new Font("Century Gothic", parametre.getChoixFont()));
    	_niveau1.setFont(new Font("Century Gothic", parametre.getChoixFont()));
    	_niveau2.setFont(new Font("Century Gothic", parametre.getChoixFont()));
    	_niveau3.setFont(new Font("Century Gothic", parametre.getChoixFont()));
    	// set listener quand changer la taille du mot
		_font.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number newValue) {
            	parametre.setChoixFont(newValue.intValue());
            	_mot1.setFont(new Font("Century Gothic", parametre.getChoixFont()));
            	_mot2.setFont(new Font("Century Gothic", parametre.getChoixFont()));
            	_mot3.setFont(new Font("Century Gothic", parametre.getChoixFont()));
            	_niveau1.setFont(new Font("Century Gothic", parametre.getChoixFont()));
            	_niveau2.setFont(new Font("Century Gothic", parametre.getChoixFont()));
            	_niveau3.setFont(new Font("Century Gothic", parametre.getChoixFont()));
            }
	    });
		
		// set background selected
		switch (parametre.getChoixImage()) {
		case 1:
			_image1.setSelected(true);
			_backPane.setId("set1");
			break;
		case 2:
			_image2.setSelected(true);
			_backPane.setId("set2");
			break;
		case 3:
			_image3.setSelected(true);
			_backPane.setId("set3");
			break;
		default:
			break;
		}
		
		// set level selected
		switch (parametre.getChoixNiveau()) {
		case 1:
			_niveau1.setSelected(true);
			break;
		case 2:
			_niveau2.setSelected(true);
			break;
		case 3:
			_niveau3.setSelected(true);
			break;
		default:
			break;
		}
		
	}


	// on fait trois methodes pour paremetrag d'image
	@FXML
	public void image1() {
		parametre.setChoixImage(1);
		_backPane.setId("set1");
		MediaPlayer startPlayer = new MediaPlayer(new Media(this.getClass().getResource("/select.mp3").toString()));
		startPlayer.play();
	}
	@FXML
	public void image2() {
		parametre.setChoixImage(2);
		_backPane.setId("set2");
		MediaPlayer startPlayer = new MediaPlayer(new Media(this.getClass().getResource("/select.mp3").toString()));
		startPlayer.play();
	}
	@FXML
	public void image3() {
		parametre.setChoixImage(3);
		_backPane.setId("set3");
		MediaPlayer startPlayer = new MediaPlayer(new Media(this.getClass().getResource("/select.mp3").toString()));
		startPlayer.play();
	}
	

	//on fait trois methodes pour parametrage du niveau
	@FXML
	public void niveau1() {
		parametre.setChoixNiveau(1);
		MediaPlayer startPlayer = new MediaPlayer(new Media(this.getClass().getResource("/select.mp3").toString()));
		startPlayer.play();
	}
	@FXML
	public void niveau2() {
		parametre.setChoixNiveau(2);
		MediaPlayer startPlayer = new MediaPlayer(new Media(this.getClass().getResource("/select.mp3").toString()));
		startPlayer.play();
	}
	@FXML
	public void niveau3() {
		MediaPlayer startPlayer = new MediaPlayer(new Media(this.getClass().getResource("/niveau3.mp3").toString()));
		startPlayer.play();
		//construire un dialog pour confimer le choix
		Dialog<GridPane> dialog = new Dialog<>();
		dialog.setTitle("!!! RED WARNING !!!");
		
		//creer un icon pour le dialog
		Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image(this.getClass().getResource("/devil_icon.png").toString()));
		
		ButtonType buttonOK = new ButtonType("Yes I'm sure", ButtonData.OK_DONE);
		ButtonType buttonNo = new ButtonType("Wait a minute..", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(buttonOK, buttonNo);
		
		GridPane pNull = new GridPane();
		Label text = new Label("Are you sure changing to the expert level ?    ");
		text.setFont(new Font("Century Gothic", parametre.getChoixFont()));
		ImageView img = new ImageView(this.getClass().getResource("/devil.png").toString());
		img.setStyle("-fx-effect: dropshadow(three-pass-box, #bb1810, 30, 0.5, 0, 0);");
		pNull.add(text, 0, 0);
		pNull.add(img, 1, 0);
		
		dialog.getDialogPane().setContent(pNull);
		dialog.setResultConverter(dialogButton->{
			if(dialogButton == buttonOK) {
				parametre.setChoixNiveau(3);
				MediaPlayer confirm = new MediaPlayer(new Media(this.getClass().getResource("/confirm.mp3").toString()));
				confirm.play();
			}
			else {
				if(parametre.getChoixNiveau() == 1)
					_niveau1.setSelected(true);
				if(parametre.getChoixNiveau() == 2)
					_niveau2.setSelected(true);
			}
			return null;
		});
		dialog.showAndWait();
		
		
	}

	public GridPane setting(GridPane p) {
		GridPane grid = p;
		
		//background
		switch (parametre.getChoixImage()) {
		case 1:
			grid.setId("pane1");
			break;
		case 2:
			grid.setId("pane2");
			break;
		case 3:
			grid.setId("pane3");
			break;
		default:
			break;
		}
		
		return grid;
	}
	
	
	
	
}
