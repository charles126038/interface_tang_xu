package principale;

import java.io.IOException;

import Controler.Option_setting;
import Controler.dictionnaireSelecteur;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application{

	
	@Override
	public void start(Stage primaryStage) throws IOException{
		
		try {
			Option_setting parametre = new Option_setting();
			dictionnaireSelecteur dictionnaire = new dictionnaireSelecteur(parametre.getChoixNiveau());
			GestionJeu jeu = new GestionJeu(dictionnaire.fichierCategorie());
			//ajouter  le fichier Home.FXML 
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
			//new un objet fenetrePrincipale
			fenetrePrincipale home = new fenetrePrincipale(jeu, primaryStage, parametre, dictionnaire);
			//ajouter le controller a home 
			loader.setController(home);
			GridPane grille = loader.load();
			
			Scene scene = new Scene(grille);
			scene.getStylesheets().addAll(this.getClass().getResource("style.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args) {
		launch(args);
	}

}
