package principale;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import Controler.Option_setting;
import Controler.dictionnaireSelecteur;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;
import option.ButtonSetting;
import option.Play;

public class fenetrePrincipale {
	/**
	 * Les buttons dans le parametre
	 */
	private int choixImage; //on a trois theme ,pour chaque theme ,on definit trois int variables
	private int choixNiveau; //changer le difficulte  (1-beginner, 2-normal, 3-export)
	private int choixFont; //changer le size du mot entre (15 - 30)
	private Option_setting parametre;
	private dictionnaireSelecteur selector;
	
	//on definit la variable de musique de fond
	private MediaPlayer backgroundMusic;
	
	private GestionJeu jeu;
	private Stage primaryStage;
	
	
	 // le gridpane sur la root
	@FXML private GridPane _fxGridPane;  
	// les 3 boutons
	@FXML private Button _play;
	@FXML private Button _setting;
	@FXML private Button _quit;
	// contacter
	@FXML private Label _nom1;
	@FXML private Label _nom2;
	@FXML private Label _email;
	// la root
	@FXML private GridPane _root;
	// les regle
	@FXML private Label _regle;
	@FXML private Label _regle2;
	// la musique
	@FXML private Button _music;
	
	@FXML private ImageView _pendu;
	public fenetrePrincipale(GestionJeu jeu, Stage primaryStage, Option_setting parametre, dictionnaireSelecteur selecteur) {
		super();
		this.parametre = parametre;
		this.choixImage = parametre.getChoixImage();
		this.choixFont = parametre.getChoixFont();
		this.choixNiveau = parametre.getChoixNiveau();
		this.selector = selecteur;
		this.jeu = jeu;
		this.primaryStage = primaryStage;
	}
	
	@FXML
	private void initialize() {
		_pendu.setImage(new Image(this.getClass().getResource("/pendu.png").toString()));
		URL musicLigne = null;
		Media music = null;
		_music.setId("music_on");
		_play.setFont(new Font("Century Gothic", choixFont));
		_setting.setFont(new Font("Century Gothic", choixFont));
		_quit.setFont(new Font("Century Gothic", choixFont));
		_nom1.setFont(new Font("Century Gothic", choixFont));
		_nom2.setFont(new Font("Century Gothic", choixFont));
		_email.setFont(new Font("Century Gothic", choixFont));
		//on choisit l'image par la variable choixImage
		switch (choixImage) {
		case 1:
			//setId
			_play.setId("button1");
			_setting.setId("button1");
			_quit.setId("button1");
			_root.setId("pane1");
			//musique
			musicLigne = getClass().getResource("/music1_1.mp3");
			music = new Media(musicLigne.toString());
			break;
		case 2:
			//setId
			_play.setId("button2");
			_setting.setId("button2");
			_quit.setId("button2");
			_root.setId("pane1");
			//musique
			musicLigne = getClass().getResource("/music2_1.mp3");
			music = new Media(musicLigne.toString());
			break;
		case 3:
			//setId
			_play.setId("button3");
			_setting.setId("button3");
			_quit.setId("button3");
			_root.setId("pane1");
			//musique
			musicLigne = getClass().getResource("/music3_1.mp3");
			music = new Media(musicLigne.toString());
			break;
		default:
			break;
		}
		
		backgroundMusic = new MediaPlayer(music);
		backgroundMusic.play();
		backgroundMusic.setAutoPlay(true);
	}

	
// la methode return la valeur de choixImage
	public int getChoixImage() {
		return choixImage;
	}

//la methode return la valeur de choixNiveau
	public int getChoixNiveau() {
		return choixNiveau;
	}

//la methode return la valeur de choixFont
	public int getChoixFont() {
		return choixFont;
	}



	@FXML
	public void play() throws IOException {
		backgroundMusic.stop();
		MediaPlayer startPlayer = new MediaPlayer(new Media(this.getClass().getResource("/start.mp3").toString()));
		startPlayer.play();
		
		Play play = new Play(parametre, jeu, selector, primaryStage);
		
		// Afficher des regles dans dialog 
		Dialog<GridPane> dialog = new Dialog<>();
		FXMLLoader Introduction = new FXMLLoader(getClass().getResource("/option/Regle.fxml"));
		Introduction.setController(this);
		
		GridPane reglePane = Introduction.load(); 
		// on eteint la musique de page HOME
		backgroundMusic.stop();   
		switch (choixImage) {
		case 1:
			reglePane.setId("pane1");
			break;
		case 2:
			reglePane.setId("pane2");
			break;
		case 3:
			reglePane.setId("pane3");
			break;
		default:
			break;
		}
		
		_regle.setFont(new Font("Century Gothic", choixFont));
		_regle2.setFont(new Font("Century Gothic", choixFont));
		dialog.getDialogPane().setContent(reglePane);
		ButtonType buttonTypeOk = new ButtonType("Let's Start!", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
		dialog.showAndWait();
		
		
		
		// on change la fenetre 
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/option/Play.fxml"));
		loader.setController(play);
		GridPane grille = null;
		try {
			grille = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// on ajoute un nouveau boutton give up
		Button abandon = new Button("Give up");
		grille.add(abandon, 2, 3);
		
		Scene scene = new Scene(grille);
		scene.getStylesheets().addAll(this.getClass().getResource("style.css").toExternalForm());
		primaryStage.setScene(scene);
		
		//car on a trois choix de choixImage
		switch (choixImage) {
		case 1:
			abandon.setId("button1");
			break;
		case 2:
			abandon.setId("button2");
			break;
		case 3:
			abandon.setId("button3");
			break;
		default:
			break;
		}
		this.backgroundMusic = play.getBackgroundMusic();
		// setaction pour des boutons
		abandon.setFont(new Font("Century Gothic", choixFont));
		abandon.setOnAction((ActionEvent)->{
			System.exit(0);
		});
		abandon.setCursor(Cursor.HAND);
		
	}
	
	@FXML
	public void setting() {
		MediaPlayer setPlayer = new MediaPlayer(new Media(this.getClass().getResource("/setting.mp3").toString()));
		setPlayer.play();
		Dialog<GridPane> dialog = new Dialog<>();
		dialog.setTitle("SETTING");
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/option/Setting.fxml"));
		ButtonSetting d = new ButtonSetting(parametre);
		
		
		loader.setController(d);
		GridPane grille = null;
		try {
			grille = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		dialog.getDialogPane().setContent(grille);
		ButtonType buttonTypeOk = new ButtonType("Ok", ButtonData.OK_DONE);
		ButtonType buttonTypeBack = new ButtonType("Back", ButtonData.CANCEL_CLOSE);
		ButtonType buttonTypeInit = new ButtonType("Default", ButtonData.APPLY);
		
		dialog.getDialogPane().getButtonTypes().addAll(buttonTypeOk, buttonTypeBack, buttonTypeInit);

		dialog.setResultConverter(new Callback<ButtonType, GridPane>() {
		    @Override
		    public GridPane call(ButtonType b) {
		        if (b == buttonTypeOk) {
		            choixImage = parametre.getChoixImage();
		            choixFont = parametre.getChoixFont();
		            choixNiveau = parametre.getChoixNiveau();
		            return d.setting(_fxGridPane);
		        }
		        else if (b == buttonTypeBack) {
		        	return null;
		        }
		        else {  
		        	// initialiser tous les choses par defaut
		        	choixImage = 3;
		    		choixNiveau = 1;
		    		choixFont = 21;
					parametre.setChoixImage(choixImage);
					parametre.setChoixFont(choixFont);
					parametre.setChoixNiveau(choixNiveau);
					initialize();
					return d.setting(_fxGridPane);
				}
		    }
		});
        
		Optional<GridPane> result = dialog.showAndWait();
		if (result.isPresent()) {
			_fxGridPane = result.get();
		}
		backgroundMusic.stop();
		initialize();
	}
	
	
	@FXML
    private void handleQuitButtonAction(ActionEvent event) {
        System.exit(0);
    }


	@FXML
	private void Music() {
		if(backgroundMusic.isMute()) {
			_music.setId("music_on");
			backgroundMusic.setMute(false);
		}
		else {
			_music.setId("music_off");
			backgroundMusic.setMute(true);
		}
	}

	
	
}
