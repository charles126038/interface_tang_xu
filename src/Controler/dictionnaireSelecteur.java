package Controler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;


public class dictionnaireSelecteur {
	private int choixNiveau;  //changer le difficulte  (1-beginner, 2-normal, 3-export)
	private Random selector;  // choisir une categorie
	private String Categorie;  // la categorie choisie
	private final ArrayList<String> Dictionnaire;  // liste des categories
	
	
	public dictionnaireSelecteur(int choixNiveau) {
		super();
		this.choixNiveau = choixNiveau;
		this.selector = new Random();
		Dictionnaire = new ArrayList<>();
		Dictionnaire.add("Animal.txt");
		Dictionnaire.add("Family.txt");
		Dictionnaire.add("Fruit.txt");
		Dictionnaire.add("Occupation.txt");
		Dictionnaire.add("Sport.txt");
		Dictionnaire.add("Time.txt");
		Dictionnaire.add("Body.txt");
		Dictionnaire.add("Color.txt");
		Dictionnaire.add("Food.txt");
	}
	/**
	 * ensemble des mots a deviner
	 * @return
	 * @throws IOException
	 */
	public String fichierCategorie() throws IOException {
		String chemin = "";
		File dictionary = null;
		switch (choixNiveau) {
		case 1:
			chemin = "/easy";
			break;
		case 2:
			chemin = "/normal";
			break;
		case 3:
			chemin = "/hard";
			break;
		default:
			break;
		}
		
		URL url = getClass().getResource(chemin);
		if(url.getProtocol().equals("jar")) {  // si on est dans le fichier .jar
			try {
				String cate = Dictionnaire.get(selector.nextInt(9));
				InputStream input = getClass().getResourceAsStream(chemin+"/"+cate);
				dictionary = File.createTempFile("temp", ".tmp");
				OutputStream out = new FileOutputStream(dictionary);
				int read;
				byte[] bytes = new byte[1024];

	            while ((read = input.read(bytes)) != -1) {
	                out.write(bytes, 0, read);
	            }
	            out.close();
	            int dot = cate.lastIndexOf('.');
				Categorie = cate.substring(0, dot); // enregistrer le nom de la categorie
				chemin = dictionary.getAbsolutePath();
			    
	            dictionary.deleteOnExit();
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		else { // si on est dans IDE
			chemin = url.getPath();
			dictionary = new File(url.getFile());
			
			File list[] = dictionary.listFiles();
			File fs = list[selector.nextInt(list.length)]; // choisir une categorie aleatoire
			chemin = chemin+"/"+fs.getName();
			int dot = fs.getName().lastIndexOf('.');
			Categorie = fs.getName().substring(0, dot); // enregistrer le nom de la categorie
			
			
		}
		return chemin;
	}

	public String getCategorie() {
		return Categorie;
	}
	
	
	
}
