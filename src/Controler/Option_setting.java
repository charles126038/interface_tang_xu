package Controler;

public class Option_setting {
	/**
	 * Les buttons dans le parametre
	 */
	private int choixImage; //changer le theme (1, 2, 3)
	private int choixNiveau; //changer le difficulte  (1-beginner, 2-normal, 3-export)
	private int choixFont; //changer le size du mot (15 - 30)
	
	public Option_setting() { // initialiser
		super();
		this.choixImage = 3;
		this.choixNiveau = 1;
		this.choixFont = 21;
	}

	public int getChoixImage() {
		return choixImage;
	}

	public void setChoixImage(int choixImage) {
		this.choixImage = choixImage;
	}

	public int getChoixNiveau() {
		return choixNiveau;
	}

	public void setChoixNiveau(int choixNiveau) {
		this.choixNiveau = choixNiveau;
	}

	public int getChoixFont() {
		return choixFont;
	}

	public void setChoixFont(int choixFont) {
		this.choixFont = choixFont;
	}
	
	
}
