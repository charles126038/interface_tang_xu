package Controler;


import java.io.IOException;
import java.net.URL;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import option.Play;
import principale.GestionJeu;

public class perdreControl {
	private int choixImage; //changer le theme (1, 2, 3)
	private int choixFont; //changer le size du mot (15 - 30)
	private String mot;  // mot a deviner
	private MediaPlayer backgroundMusic;
	
	private Option_setting parametre;
	private dictionnaireSelecteur selector;
	private Stage primaryStage;
	private GestionJeu jeu;
	
	@FXML GridPane _pane;
	@FXML Button _again;
	@FXML Button _quit;
	@FXML ImageView _img;
	@FXML Label _mot;
	
	public perdreControl(Option_setting parametre, String motMystere, Stage primaryStage) throws IOException{
		super();
		mot = motMystere;
		this.parametre = parametre;
		
		this.choixImage = parametre.getChoixImage();
		this.choixFont = parametre.getChoixFont();
		
		this.selector = new dictionnaireSelecteur(parametre.getChoixNiveau());
		this.jeu = new GestionJeu(selector.fichierCategorie());
		
		this.primaryStage = primaryStage;
		
	}
	
	@FXML
	private void initialize() {
		URL musicLigne = null;
		Media music = null;
		
		Image stickman = null;
		_again.setFont(new Font("Century Gothic", choixFont));
		_quit.setFont(new Font("Century Gothic", choixFont));
		switch (choixImage) {
		case 1:
			_pane.setId("pane1");
			_again.setId("button1");
			_quit.setId("button1");
			stickman = new Image(this.getClass().getResource("/perdre1.png").toString());
			musicLigne = getClass().getResource("/lose1.mp3");
			music = new Media(musicLigne.toString());
			break;
		case 2:
			_pane.setId("pane2");
			_again.setId("button2");
			_quit.setId("button2");
			stickman = new Image(this.getClass().getResource("/perdre2.png").toString());
			musicLigne = getClass().getResource("/lose2.mp3");
			music = new Media(musicLigne.toString());
			break;
		case 3:
			_pane.setId("pane3");
			_again.setId("button3");
			_quit.setId("button3");
			stickman = new Image(this.getClass().getResource("/perdre3.png").toString());
			musicLigne = getClass().getResource("/lose3.mp3");
			music = new Media(musicLigne.toString());
			break;
		default:
			break;
		}
		_img.setImage(stickman);
		_img.setStyle("-fx-effect: dropshadow(three-pass-box, white, 30, 0.4, 0, 0);");
		_mot.setText("The word is "+mot+" ...");
		_mot.setFont(new Font("Century Gothic", choixFont));
		
		backgroundMusic = new MediaPlayer(music);
		backgroundMusic.play();
	}
	


	@FXML
	private void again() throws IOException{
		
		Play play = new Play(parametre, jeu, selector, primaryStage);
		
		// changer fenetre
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/option/Play.fxml"));
		loader.setController(play);
		GridPane grille = null;
		try {
			grille = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// nouveau boutton
		Button abandon = new Button("Give up");
		grille.add(abandon, 2, 3);
		//initialiser le button Abandon
		switch (choixImage) {
		case 1:
			abandon.setId("button1");
			break;
		case 2:
			abandon.setId("button2");
			break;
		case 3:
			abandon.setId("button3");
			break;
		default:
			break;
		}
		// set action des bouttons
		abandon.setFont(new Font("Century Gothic", choixFont));
		abandon.setOnAction((ActionEvent)->{
			System.exit(0);
		});
		abandon.setCursor(Cursor.HAND);
		Scene scene = new Scene(grille);
		scene.getStylesheets().addAll(this.getClass().getResource("/principale/style.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	@FXML
	private void quit() {
		System.exit(0);
	}
}
